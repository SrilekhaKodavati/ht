import { useState } from 'react';

import Layout from '../../components/Layout';

import axios from 'axios';

import { auth } from '../Auth/firebase';
import { useAuthState } from 'react-firebase-hooks/auth';

import {
  Container,
  Heading,
  useToast,
  Text,
  Select,
  VStack,
  HStack,
  Table,
  Thead,
  Tbody,
  Tfoot,
  Tr,
  Th,
  Td,
  TableCaption,
  TableContainer,
} from '@chakra-ui/react';

var moment = require('moment'); // require

const BASE_URL = 'http://localhost:4000';

export default function Leaderboard() {
  const toast = useToast();
  const [user, loading] = useAuthState(auth);
  const [loaded, setLoaded] = useState(false);
  const [userData, setUserData] = useState([]);
  const [chosenHabit, setChosenHabit] = useState('');

  const getAllHabits = async () => {
    if (!loading) {
      await axios
        .get(`${BASE_URL}/api/users`)
        .then(function (response) {
          setUserData(response.data.users);
        })
        .catch(function (err) {
          console.log(err);
          toast({
            title: 'Error occured.',
            description: 'Could not get details of users.',
            status: 'error',
            duration: 9000,
            isClosable: true,
          });
        });
    }
  };

  const getDatesInRange = (startDate, endDate) => {
    var dates = [];
    var currDate = moment(startDate).startOf('day');
    var lastDate = moment(endDate).startOf('day');
    while (currDate.add(1, 'days').diff(lastDate) < 1) {
      dates.push(currDate.clone().toDate());
    }
    return dates;
  };

  const calculateStreak = (stateDict) => {
    const dateArr = Object.keys(stateDict).map(
      (dateString) => new Date(dateString)
    );

    dateArr.sort(function (a, b) {
      return b - a;
    });
    const allDates = getDatesInRange(dateArr[dateArr.length - 1], new Date()); // State object in mongo doesn't have all dates. this array fixes that.
    allDates.reverse();

    let currentStreak = 0;

    for (let i = 0; i < allDates.length; i++) {
      let dateString = moment(allDates[i]).format('YYYY-MM-DD');
      if (!dateString in stateDict) {
        return currentStreak;
      } else if (stateDict[dateString] === 1) {
        currentStreak++;
      } else {
        return currentStreak;
      }
    }
    return currentStreak;
  };

  if (!loading) {
    if (!loaded) {
      getAllHabits();
      setLoaded(true);
    } else {
      const uniqueHabits = new Set();
      userData.map((user) =>
        user.habits.filter(habit => habit.type === 'binary').map((habit) => uniqueHabits.add(habit['name']))
      );
      return (
        <Layout user={user} currentRoute='leaderboard'>
          <Container centerContent minH='80vh' justifyContent='center'>
            <VStack spacing={8}>
              <Heading>Leaderboard</Heading>
              <Text>Here you will find the highest streak maintainers for each habit.</Text>
              <HStack>
                <Text>Habit:</Text>
                <Select
                  onChange={(event) => setChosenHabit(event.target.value)}
                  placeholder='Select'
                  value={chosenHabit}
                >
                  {Array.from(uniqueHabits).map((habit) => (
                    <option key={habit} value={habit}>
                      {habit}
                    </option>
                  ))}
                </Select>
              </HStack>
              <TableContainer>
                <Table variant='striped'>
                  <Thead>
                    <Tr>
                      <Th>Username</Th>
                      <Th>Streak</Th>
                    </Tr>
                  </Thead>
                  <Tbody>
                    {userData.map((user) =>
                      user.habits
                        .filter((habit) => habit.name == chosenHabit)
                        .map((habit) => (
                          <Tr key={user.userName}>
                            <Td>{user.userName}</Td>
                            <Td>{calculateStreak(habit.state)}</Td>
                          </Tr>
                        ))
                    )}
                  </Tbody>
                </Table>
              </TableContainer>
            </VStack>
          </Container>
        </Layout>
      );
    }
  }
}
