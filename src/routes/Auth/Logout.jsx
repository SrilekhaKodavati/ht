import Layout from '../../components/Layout';
import React, { useEffect, useState } from 'react';
import { useAuthState } from 'react-firebase-hooks/auth';
import { useNavigate } from 'react-router-dom';
import { auth, db, logout } from './firebase';
import { query, collection, getDocs, where } from 'firebase/firestore';
import { Heading, Button, Text, VStack, Container } from '@chakra-ui/react';

export default function Logout() {
  const [user, loading] = useAuthState(auth);
  const [setName] = useState('');
  const navigate = useNavigate();

  const fetchUserName = async () => {
    try {
      const q = query(collection(db, 'users'), where('uid', '==', user?.uid));
      const doc = await getDocs(q);
      const data = doc.docs[0].data();

      setName(data.name);
    } catch (err) {
      console.error(err);
    }
  };

  useEffect(() => {
    if (loading) return;
    if (!user) return navigate('/');

    fetchUserName();
  }, [user, loading]);

  const nologout = () => {
    return navigate('../Home');
  };

  if (!loading) {
    return (
      <>
        <Layout currentRoute='logout' user={user}>
          <Container centerContent minH='80vh' justifyContent='center'>
            <VStack
              align='left'
              p={12}
              borderRadius={8}
              boxShadow='lg'
              spacing={8}
            >
              <Heading>Log Out</Heading>
              <Text>Do you want to log out?</Text>
              <VStack>
                <Button colorScheme='teal' minW='100%' onClick={logout}>
                  {' '}
                  Yes
                </Button>
                <Button colorScheme='teal' minW='100%' onClick={nologout}>
                  {' '}
                  No
                </Button>
              </VStack>
            </VStack>
          </Container>
        </Layout>
      </>
    );
  }
}
