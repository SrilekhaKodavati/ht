import { useState } from 'react';

import Layout from '../../components/Layout';
import axios from 'axios';

import { auth } from '../Auth/firebase';
import { useAuthState } from 'react-firebase-hooks/auth';

import {
  HStack,
  VStack,
  useToast,
  Link,
  Center,
  Heading,
  FormControl,
  Button,
  Stack,
  Box,
  Card,
  CardBody,
  SimpleGrid,
  Text,
  Input
} from '@chakra-ui/react'

const BASE_URL = 'http://localhost:4000';

export default function Friends({ currentRoute }) {
  const toast = useToast();
  const [userFriendData, setUserFriendData] = useState([]);
  const [user, loading] = useAuthState(auth);
  const [loaded, setLoaded] = useState(false);
  let [searchTerm, setSearchTerm] = useState(".");



  const getAllFriends = async () => {
    if (!loading) {
      await axios
        .get(`${BASE_URL}/api/users/${user.email}/friends`)
        .then(function (response) {
          setUserFriendData(response.data.friends_name_email);
          // console.log(response.data.friends_name_email);
          // console.log(userFriendData);
        })
        .catch(function (err) {
          console.log(err);
          toast({
            title: 'Error occured.',
            description: 'Could not get details of users.',
            status: 'error',
            duration: 9000,
            isClosable: true,
          });
        });
    }
  };


  //Search Functionality
  const getSearchedFriends = (event) => {
    setSearchTerm(event.target.value);
    // console.log(event.target.value);
  }

  //Unfollow friend Functionality
  const UnfollowFriend = async (e) => {
    e.preventDefault();
    let button_index = Number(e.target.id);   //index is stored in id field
    // console.log(button_index);

    //using index fetch that friend email id
    let friend_email_id = userFriendData[button_index][0];
    // console.log(friend_email_id);

    //call update api

    await axios
      .put(
        `${BASE_URL}/api/users/${user.email}/friends`,
        { friendEmail: friend_email_id }
      )
      .then(function (response) {
        setLoaded(false);

      })
      .catch(function (err) {
        console.log(err);
        toast({
          title: 'Error occured.',
          description: 'Could not unfollow user.',
          status: 'error',
          duration: 9000,
          isClosable: true,
        });
      });

  }



  if (!loading) {
    if (!loaded) {
      getAllFriends();
      setLoaded(true);
    }
    return (
      <Layout user={user} currentRoute='friends'>
        <Center h='100%' >
          <Box sx={{ width: '70%' }}>
            <br></br><br></br>
            <FormControl>
              <Stack direction='row' justifyContent='center' spacing={5}>
                <Heading as='h1' size='lg' noOfLines={1}>All Friends</Heading>
              </Stack>
              <br></br>
              <Stack direction='row' justifyContent='space-between' spacing={25}>
                <Input variant='filled' htmlSize={70} width='auto' placeholder='Search for Friend' id="message" onChange={getSearchedFriends} />
                <HStack>
                  {['Find Friends'].map(
                    (routeName) => (
                      <Link
                        href={`/${routeName.replace(/\s/g, '').toLowerCase()}`}
                        key={routeName}
                        style={{ textDecoration: 'none' }}>
                        <Button
                          borderRadius={50}
                          colorScheme='gray'
                          variant={
                            currentRoute === routeName.toLowerCase() ? 'solid' : 'outline'
                          }>
                          {routeName}
                        </Button>
                      </Link>
                    ))}
                </HStack>
              </Stack>
            </FormControl>
            <br></br><br></br>
            <VStack spacing={70}>
              <ul>
                {userFriendData.length >= 1 && userFriendData.filter((elem) => elem[1].match(new RegExp(searchTerm, 'gi'))).map((item, index) => {
                  return (
                    <SimpleGrid columns={1} >
                      <Card><CardBody>
                        <Stack direction='row' justifyContent='space-between' spacing={400}>
                          <Text>{item[1]} <Text>({item[0]})</Text></Text>

                          <Button id={index} colorScheme='red' variant='solid' size='md' onClick={UnfollowFriend} > Unfollow </Button></Stack>
                      </CardBody></Card>
                    </SimpleGrid>
                  )
                })}
              </ul>
            </VStack>
            {userFriendData.length === 0 && (
              <Heading fontSize='xl' textAlign='center'>
                You currently have no friends. Click on the "Find Friends" button to
                add friends.
              </Heading>
            )}
          </Box>
        </Center>
      </Layout>
    );
  }
}


