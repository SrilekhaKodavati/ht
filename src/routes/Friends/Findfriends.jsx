import { useState } from 'react';

import Layout from '../../components/Layout';
import axios from 'axios';

import { auth } from '../Auth/firebase';
import { useAuthState } from 'react-firebase-hooks/auth';

import {
  HStack,
  VStack,
  useToast,
  Link,
  Center,
  Heading,
  FormControl,
  Button,
  Stack,
  Box,
  Card,
  CardBody,
  SimpleGrid,
  Text,
  Input
} from '@chakra-ui/react'

const Users =
{
  "id": '01',
  "name": 'Brad Pitt',
  "Friends": ["sam1", "sam2", "sam3", "sam4", "sam5"],
  "Friendrequest": ["lan1", "lan2", "lan3", "lan4", "lan5"]
}



const BASE_URL = 'http://localhost:4000';


export default function Friendfind() {

  const toast = useToast();
  const [userNotFriendData, setUserNotFriendData] = useState([]);
  const [user, loading] = useAuthState(auth);
  const [loaded, setLoaded] = useState(false);
  let [searchTerm, setSearchTerm] = useState(".");


  const getAllNotFriends = async () => {
    if (!loading) {
      await axios
        .get(`${BASE_URL}/api/users/${user.email}/notfriends`)
        .then(function (response) {
          setUserNotFriendData(response.data.not_friends_name_email);
          // console.log(response.data.friends_name_email);
          // console.log(userFriendData);
        })
        .catch(function (err) {
          console.log(err);
          toast({
            title: 'Error occured.',
            description: 'Could not get details of users.',
            status: 'error',
            duration: 9000,
            isClosable: true,
          });
        });
    }
  };

  //Search Functionality
  const getSearchedFriends = (event) => {
    setSearchTerm(event.target.value);
    // console.log(event.target.value);
  }

  //Follow friend Functionality
  const FollowFriend = async (e) => {
    e.preventDefault();
    let button_index = Number(e.target.id);   //index is stored in id field
    // console.log(button_index);

    //using index fetch that friend email id
    let friend_email_id = userNotFriendData[button_index][0];
    // console.log(friend_email_id);

    //call update api

    await axios
      .put(
        `${BASE_URL}/api/users/${user.email}/notfriends`,
        { friendEmail: friend_email_id }
      )
      .then(function (response) {
        setLoaded(false);

      })
      .catch(function (err) {
        console.log(err);
        toast({
          title: 'Error occured.',
          description: 'Could not Follow user.',
          status: 'error',
          duration: 9000,
          isClosable: true,
        });
      });

  }

  if (!loading) {
    if (!loaded) {
      getAllNotFriends();
      setLoaded(true);
    }
    return (
      <Layout user={user} currentRoute='friendfind'>
        <Center h='100%' >
          <Box sx={{ width: '70%' }}>
            <FormControl>
              <Stack direction='row' justifyContent='center' spacing={5}>
                <Heading as='h1' size='lg' noOfLines={1}>Find Friends</Heading>
              </Stack>
              <br></br>
              <Stack direction='row' justifyContent='center' spacing={5}>
                <Input variant='filled' htmlSize={50} width='auto' placeholder='Search for Friend' id="message" onChange={getSearchedFriends} />
              </Stack>
            </FormControl>
            <br></br><br></br>
            <VStack spacing={70}>
              <ul>
                {userNotFriendData.length >= 1 && userNotFriendData.filter((elem) => elem[1].match(new RegExp(searchTerm, 'gi'))).map((item, index) => {
                  return (
                    <SimpleGrid columns={1} >
                      <Card><CardBody>
                        <Stack direction='row' justifyContent='space-between' spacing={400}>
                          <Text>{item[1]} <Text>({item[0]})</Text></Text>
                          <Button id={index} colorScheme='green' variant='solid' size='md' onClick={FollowFriend}> Follow </Button></Stack>
                      </CardBody></Card>
                    </SimpleGrid>
                  )
                })}
              </ul>
            </VStack>
            {userNotFriendData.length === 0 && (
              <Heading fontSize='xl' textAlign='center'>
                You currently are following all the users registered on the application.
              </Heading>
            )}
          </Box>
        </Center>
      </Layout>

    );
  }
}