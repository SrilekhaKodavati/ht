import moment from 'moment/moment';

import { Flex, Grid, HStack, Text, VStack } from '@chakra-ui/react';

import HabitRow from './HabitRow';
import { numDaysToGoBack, squareSideLen } from './HabitRow';

export default function HabitGrid({ user, habits, habitsChangeHandler }) {
  const gridItems = [];
  for (let i = 0; i < habits.length; i++) {
    gridItems.push(
      <HabitRow
        user={user}
        key={i}
        index={i}
        habits={habits}
        habitsChangeHandler={habitsChangeHandler}
      />
    );
  }

  const dateDays = [];
  var latestDate = new Date();
  var earliestDate = new Date(
    latestDate.getTime() - numDaysToGoBack * 24 * 60 * 60 * 1000
  );
  for (let i = 0; i <= numDaysToGoBack; i++) {
    const date = new Date(earliestDate.getTime() + i * 24 * 60 * 60 * 1000);
    dateDays.push(date);
  }

  const dateRow = dateDays.map((date) => {
    const isToday = moment(date).isSame(new Date(), 'day');
    return (
      <VStack
        spacing={0}
        w={squareSideLen}
        h={75}
        align='center'
        justify='center'
        key={date}
      >
        <Text
          fontFamily='Inconsolata'
          fontSize='xl'
          color={isToday ? 'green.500' : 'black'}
          as={isToday ? 'b' : ''}
        >
          {moment(date).format('DD')}
        </Text>
        <Text
          fontFamily='Inconsolata'
          fontSize='md'
          color={isToday ? 'green.500' : 'gray'}
          as={isToday ? 'b' : ''}
        >
          {moment(date).format('ddd')}
        </Text>
      </VStack>
    );
  });

  gridItems.splice(
    0,
    0,
    <HStack key={'this-is-just-to-supress-a-warning-lol'}>
      <Flex minW='10rem' ml={5}></Flex>
      {dateRow}
    </HStack>
  );

  return <Grid gap={2}>{gridItems}</Grid>;
}