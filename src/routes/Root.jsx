import Layout from '../components/Layout1';
import { Link, useNavigate } from "react-router-dom";
import {Flex,Container,Heading,HStack,Stack,Text,Button,Icon,Box,Image,IconProps,} from '@chakra-ui/react';


export default function Root() {
  const Habit = () => {
    return (
      <Box w = '200rem' h = '10rem'>
          <Image src='habit.png' alt='habit' />
      </Box>
    );
  };
  const Calender = () => {
    return (
      <Box w = '20rem' h = '20rem'>
      <br></br><br></br>
          <Image src='calender.png' alt='Calender' />
      </Box>
    );
  };
    
  return (
    <Layout>
      <Container maxW={'13xl'} maxH={'xl'}>
      
      <Stack
        textAlign={'center'}
        >
        <HStack spacing={250}>
        <Container maxW={'5xl'} >
        <Heading
          fontWeight={450}
          fontSize={{ base: '4xl', sm: '5xl', md: '6xl' }}
          lineHeight={'110%'}
          color={'BlackAlpha 500'}
          >
           
          Habit Tracking{' '}
          <Text as={'span'} color={'teal.400'}>
            made easy
          </Text>
        </Heading>
        <Text color={'gray.400'} maxW={'3xl'}>
           Keep track of your
          Habits Here...
        </Text>
        <br></br>
        <Link to="../register">
          <Button
            rounded={'full'}
            px={6}
            colorScheme={'teal'}
            bg={'teal.400'}
            _hover={{ bg: 'teal.500' }} 
            >
            Get started
          </Button>
          </Link>
          </Container>
          <Container>
            <Calender/>
          </Container>
          </HStack>
        <Flex w={'full'}>
        <Habit/>
        </Flex>
      </Stack>
    </Container>
    </Layout>
  )
}
