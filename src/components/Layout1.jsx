import { Container } from '@chakra-ui/react';
import NavBar from './NavBar1';

export default function Layout({ children, currentRoute }) {
  return (
    <>
      <NavBar currentRoute={currentRoute} />
      <Container justifyContent='center' h='calc(90vh)' maxWidth='6xl'>
        {children}
      </Container>
    </>
  );
}