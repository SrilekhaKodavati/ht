import { Box, Button, HStack, Image, Link } from '@chakra-ui/react';

const Logo = () => {
  return (
    <Box boxSize='3rem'>
      <Link href='/'>
        <Image src='logo.png' alt='Logo' />
      </Link>
    </Box>
  );
};

export default function NavBar({ currentRoute }) {
  return (
    <HStack justifyContent='space-between' p={5}>
      <Logo/>
      <HStack>
        {[ 'Login','Register'].map(
          (routeName) => (
            <Link
              href={`/${routeName.replace(/\s/g, '').toLowerCase()}`}
              key={routeName}
              style={{ textDecoration: 'none' }}
            >
              <Button
                borderRadius={50}
                colorScheme='gray'
                variant={
                  currentRoute === routeName.toLowerCase() ? 'solid' : 'ghost'
                }
              >
                {routeName}
              </Button>
            </Link>
          )
        )}
      </HStack>
    </HStack>
  );
}
