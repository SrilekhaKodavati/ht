import {
  Box,
  Button,
  Heading,
  HStack,
  Image,
  Link,
  Menu,
  MenuButton,
  MenuItem,
  MenuList,
} from '@chakra-ui/react';

import { ChevronDownIcon } from '@chakra-ui/icons';

const Logo = () => {
  return (
    <Box boxSize='3rem'>
      <Link href='../home'>
        <Image src='logo.png' alt='Logo' />
      </Link>
    </Box>
  );
};

export default function NavBar({ currentRoute, user }) {
  return (
    <HStack justifyContent='space-between' p={5}>
      <HStack>
        <Logo />
        <Link href='../home' style={{ textDecoration: 'none' }}>
          <Heading size='lg' fontFamily={'Unbounded'}>
            Habit Tracker
          </Heading>
        </Link>
      </HStack>

      <HStack spacing={5}>
        {['Home', 'Leaderboard', 'Friends'].map((routeName) => (
          <Link
            href={`/${routeName.replace(/\s/g, '').toLowerCase()}`}
            key={routeName}
            style={{ textDecoration: 'none' }}
          >
            <Button
              borderRadius={50}
              colorScheme='blue'
              size='md'
              variant={
                currentRoute === routeName.toLowerCase() ? 'solid' : 'ghost'
              }
            >
              {routeName}
            </Button>
          </Link>
        ))}
        {user && (
          <Menu>
            <MenuButton
              as={Button}
              variant='link'
              colorScheme='teal'
              size='md'
              borderRadius={50}
              rightIcon={<ChevronDownIcon />}
            >
              {user.email.split('@')[0]}
            </MenuButton>
            <MenuList>
              <Link href='/logout'>
                <MenuItem>Logout</MenuItem>
              </Link>
            </MenuList>
          </Menu>
        )}
      </HStack>
    </HStack>
  );
}
